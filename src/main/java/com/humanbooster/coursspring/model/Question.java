package com.humanbooster.coursspring.model;


import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contenu")
    @Length(min = 4, max = 10, message = "Vous devez saisir entre 4 et 10 caractères")
    private String contenu;

    @OneToOne(optional = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "media_id")
    private Media media;


    @ManyToOne
    @JoinColumn(name = "domaine_id", referencedColumnName = "id")
    @NotNull(message = "Le média ne peut pas être null ! ")
    private Domaine domaine;


    public Domaine getDomaine() {
        return domaine;
    }

    public void setDomaine(Domaine domaine) {
        this.domaine = domaine;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", contenu='" + contenu + '\'' +
                ", media=" + media +
                ", domaine=" + domaine +
                '}';
    }

}
