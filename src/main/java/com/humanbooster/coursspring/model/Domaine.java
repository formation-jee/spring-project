package com.humanbooster.coursspring.model;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "domaine")
public class Domaine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String libelle;


    @OneToMany(mappedBy = "domaine", fetch = FetchType.EAGER)
    private List<Question> questions;

    public Domaine() {
        this.questions = Collections.emptyList();
    }

    public List<Question> getQuestions() {
        return this.questions;
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }

    public void removeQuestion(Question question) {
        this.questions.remove(question);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "Domaine{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", questions=" + questions +
                '}';
    }
}
