package com.humanbooster.coursspring.model;



import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "user")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "role", discriminatorType = DiscriminatorType.STRING)
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;


    @Column(name = "civilite")
    @Enumerated(EnumType.ORDINAL)
    private GenderEnum civilite;

    @Column(nullable = false)
    @NotNull(message = "Veuillez saisir un nom")
    private String nom;

    @Column
    @NotNull(message = "Veuillez saisir un prénom")
    private String prenom;

    @Column
    @NotNull(message = "Veuillez saisir un password")
    private String password;


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public GenderEnum getCivilite() {
        return civilite;
    }

    public void setCivilite(GenderEnum civilite) {
        this.civilite = civilite;
    }

}
