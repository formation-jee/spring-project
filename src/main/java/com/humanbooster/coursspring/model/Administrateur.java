package com.humanbooster.coursspring.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Admin")
public class Administrateur extends User {

    @Column(name = "numero_pro")
    private String numPro;

    public Administrateur() {
    }

    public String getNumPro() {
        return numPro;
    }

    public void setNumPro(String numPro) {
        this.numPro = numPro;
    }
}
