package com.humanbooster.coursspring.model;

public enum GenderEnum {
    M, MME, MLLE
}
