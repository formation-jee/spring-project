package com.humanbooster.coursspring.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

@Entity
public class Candidat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    @NotNull(message = "Le champ nom ne peut pas être null")
    @NotBlank(message = "Le champ ne peut pas être blanc !")
    private String nom;

    @Column(name = "prenom")
    @NotNull(message = "Le champ prénom ne peut pas être null")
    @NotBlank(message = "Le champ ne peut pas être blanc !")
    private String prenom;

    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Le champ date ne peut pas être null")
    @Past(message = "La date ne dois pas être dans le future")
    private Date dateNaissance;

    @Column(name = "adresse")
    @NotNull(message = "Veuillez saisir une adresse")
    @NotBlank(message = "Le adresse ne peut pas être blanc !")
    private String adresse;

    @Column(name = "ville")
    @NotNull(message = "Veuillez saisir une ville")
    @NotBlank(message = "Le champ ville ne peut pas être blanc !")
    private String ville;

    @Column(name = "code_postal")
    @NotNull(message = "Veuillez saisir un CP")
    @NotBlank(message = "Veuillez saisir un CP")
    private String codePostal;

    public Candidat() {
    }

    public Candidat(Long id, String nom, String prenom, Date dateNaissance, String adresse, String ville, String codePostam) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostam;
    }

    public Candidat(String nom, String prenom, Date dateNaissance, String adresse, String ville, String codePostam) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostam;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candidat candidat = (Candidat) o;

        if (id != null ? !id.equals(candidat.id) : candidat.id != null) return false;
        if (nom != null ? !nom.equals(candidat.nom) : candidat.nom != null) return false;
        if (prenom != null ? !prenom.equals(candidat.prenom) : candidat.prenom != null) return false;
        if (dateNaissance != null ? !dateNaissance.equals(candidat.dateNaissance) : candidat.dateNaissance != null)
            return false;
        if (adresse != null ? !adresse.equals(candidat.adresse) : candidat.adresse != null) return false;
        if (ville != null ? !ville.equals(candidat.ville) : candidat.ville != null) return false;
        return codePostal != null ? codePostal.equals(candidat.codePostal) : candidat.codePostal == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (prenom != null ? prenom.hashCode() : 0);
        result = 31 * result + (dateNaissance != null ? dateNaissance.hashCode() : 0);
        result = 31 * result + (adresse != null ? adresse.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Candidat{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", adresse='" + adresse + '\'' +
                ", ville='" + ville + '\'' +
                ", codePostal='" + codePostal + '\'' +
                '}';
    }
}
