package com.humanbooster.coursspring;

import com.humanbooster.coursspring.model.Candidat;
import com.humanbooster.coursspring.repository.CandidatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class CoursspringApplication {

	private Logger logger = LoggerFactory.getLogger(CoursspringApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CoursspringApplication.class, args);
	}


	@Bean
	public CommandLineRunner testData(CandidatRepository candidatRepository) {
		return (args) -> {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date birthMacron = formatter.parse("21/12/1977");

			Date birthAurelien = formatter.parse("30/03/1993");
			Date birthCastex = formatter.parse("25/06/1965");
			Date birthBardet = formatter.parse("09/11/1990");

			if(candidatRepository.count() == 0) {
				logger.info("Ici je sauvegarde 1 candidat !!!");
				candidatRepository.save(new Candidat("Macron", "Emmanuel",birthMacron, "Palais de l'Elysée", "Paris", "75000"));

				logger.info("Ici, je sauvegarde 3 candidats !");

				List<Candidat> candidatList =  new ArrayList<Candidat>();

				candidatList.add(new Candidat("Delorme", "Aurélien", birthAurelien, "Stade Marcel Michelin", "Clermont-Ferrand", "63000"));
				candidatList.add(new Candidat("Castex", "Jean", birthCastex, "21 Avenue Edmond Berges", "Vic-Fezensac", "32190"));
				candidatList.add(new Candidat("Bardet", "Romain", birthCastex, "Avenue thermale", "Royat", "63130"));

				candidatRepository.saveAll(candidatList);
			}


			List<Candidat> candidats = candidatRepository.findByNom("Macron");

			for (Candidat candidat: candidats) {
				logger.info(candidat.toString());
			}


		};
	}
}
