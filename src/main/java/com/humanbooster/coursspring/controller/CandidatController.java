package com.humanbooster.coursspring.controller;

import com.humanbooster.coursspring.model.Candidat;
import com.humanbooster.coursspring.services.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path="/candidats")
public class CandidatController {

    @Autowired
    private CandidatService candidatService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView listCandidat() {

        ModelAndView mv = new ModelAndView("candidat/list");
        List<Candidat> candidatList = this.candidatService.getAll();
        mv.addObject("candidats", candidatList);

        return mv;
    }

    @RequestMapping(value = "/{candidat}", method = RequestMethod.GET)
    public ModelAndView candidatDetail(@PathVariable(name = "candidat", required = false) Candidat candidat) {

        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        } else {
            ModelAndView mv = new ModelAndView("candidat/detail");
            mv.addObject("candidat", candidat);

            return mv;
        }
    }


    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView add() {
        Candidat candidat = new Candidat();

        ModelAndView mv = new ModelAndView("candidat/form");
        mv.addObject("candidat", candidat);

        return mv;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String submittedCandidate(@Valid  Candidat candidat, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "candidat/form";
        } else {
            this.candidatService.saveOrUpdate(candidat);
            return "redirect:/candidats/";
        }
    }


    @RequestMapping(value = "/edit/{candidat}", method = RequestMethod.GET)
    public ModelAndView editForm(@PathVariable(name = "candidat", required = false) Candidat candidat) {
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        }

        ModelAndView mv = new ModelAndView("candidat/form");
        mv.addObject("candidat", candidat);

        return mv;
    }

    @RequestMapping(value = "/edit/{candidat}", method = RequestMethod.POST)
    public String submittedEditForm(@Valid Candidat candidat, BindingResult bindingResult) {

        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        }

        if(bindingResult.hasErrors()) {
            return "candidat/form";
        } else {
            this.candidatService.saveOrUpdate(candidat);
            return "redirect:/candidats/";
        }
    }

    @RequestMapping(value = "/delete/{candidat}", method = RequestMethod.GET)
    public String deleteCandidat(@PathVariable(name = "candidat", required = false) Candidat candidat) {
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        } else {
            this.candidatService.deleteCandidat(candidat);
            return "redirect:/candidats/";
        }
    }


}
