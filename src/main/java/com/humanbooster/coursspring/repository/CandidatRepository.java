package com.humanbooster.coursspring.repository;

import com.humanbooster.coursspring.model.Candidat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidatRepository extends CrudRepository<Candidat, Long> {

    List<Candidat> findCandidatByAdresse(String adresse);

    List<Candidat> findByNom(String nom);

    @Query("SELECT c from Candidat c WHERE c.adresse = :adresse")
    List<Candidat> findWithHqlAdresse(String adresse);

    @Override
    List<Candidat> findAll();

}
