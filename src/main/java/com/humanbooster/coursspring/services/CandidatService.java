package com.humanbooster.coursspring.services;

import com.humanbooster.coursspring.model.Candidat;
import com.humanbooster.coursspring.repository.CandidatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatService {

    @Autowired
    private CandidatRepository candidatRepository;

    public List<Candidat> getAll() {
        return this.candidatRepository.findAll();
    }

    public void saveOrUpdate(Candidat candidat) {
        this.candidatRepository.save(candidat);
    }

    public void deleteCandidat(Candidat candidat) {
        this.candidatRepository.delete(candidat);
    }

}
